import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Participant from '../views/Participant.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/appointment/:gguid',
    name: 'participant',
    component: Participant
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
