function formatLocalDate (date) {
    const options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit' };
    return new Date(date).toLocaleDateString("hr-HR", options);
}

export default {
    formatDate: formatLocalDate
}
